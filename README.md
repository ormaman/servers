```
Ready
----------------------------------------------------
number	NAME	EXTERNAL_IP
1	docker-workshop-316c	34.76.92.247
2	docker-workshop-3rgb	35.189.208.218
3	docker-workshop-3trk	34.76.185.95
4	docker-workshop-3tss	35.240.17.58
5	docker-workshop-4x0w	35.241.188.115
6	docker-workshop-52p9	192.158.30.104
7	docker-workshop-bd52	34.77.154.26
8	docker-workshop-bx1q	35.195.161.20
9	docker-workshop-cwvc	35.195.106.221
10	docker-workshop-gw47	35.205.233.210
11	docker-workshop-hrx1	35.195.167.27
12	docker-workshop-jhdr	35.241.224.43
13	docker-workshop-kbrq	35.195.85.112
14	docker-workshop-mjl7	35.233.17.206
15	docker-workshop-mm76	104.199.102.166
16	docker-workshop-mrf7	35.187.96.96
17	docker-workshop-p3l1	35.205.208.81
18	docker-workshop-r0nm	34.76.78.53
19	docker-workshop-sgkh	35.187.23.170
20	docker-workshop-t9p6	35.240.75.117
21	docker-workshop-tb9q	35.187.38.47
22	docker-workshop-wqlq	35.233.84.244
----------------------------------------------------
```
```
Clean
----------------------------------------------------
Number	NAME	IP
1	docker-workshop-clean-3dr9	35.205.50.44
2	docker-workshop-clean-4mh8	35.195.102.124
3	docker-workshop-clean-581l	35.187.34.41
4	docker-workshop-clean-6txq	35.240.66.169
5	docker-workshop-clean-77kt	34.77.186.212
6	docker-workshop-clean-94f1	104.199.40.31
7	docker-workshop-clean-bl5h	34.77.217.51
8	docker-workshop-clean-cjzz	35.240.124.3
9	docker-workshop-clean-f59c	34.76.228.67
10	docker-workshop-clean-m7sv	35.241.255.229
11	docker-workshop-clean-mc53	34.77.130.228
12	docker-workshop-clean-nmxw	34.76.135.181
13	docker-workshop-clean-phdf	35.205.196.168
14	docker-workshop-clean-rqkl	104.199.29.84
15	docker-workshop-clean-td26	35.187.104.40
16	docker-workshop-clean-tj1z	34.76.94.241
17	docker-workshop-clean-v82z	35.205.5.140
18	docker-workshop-clean-wrb4	34.76.248.87
19	docker-workshop-clean-x460	35.190.210.161
20	docker-workshop-clean-xg94	35.195.133.212
21	docker-workshop-clean-z40b	34.77.0.200
22	docker-workshop-clean-zp3z	35.195.13.48
```
